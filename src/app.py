from datetime import timedelta
from glob import glob
from os import mkdir, urandom

from flask import Flask, session

from utils.config import Config

app = Flask(__name__, static_url_path="/")

# import all routes
for file in glob("*/routes/*.py"):
    module = file.replace("/", ".").split(".")[-2]
    exec(f"from routes.{module} import router as {module}")
    exec(f"app.register_blueprint({module}, url_prefix='{Config.base}')")

app.secret_key = urandom(12)


@app.before_request
def make_session_permanent():
    session.permanent = True
    app.permanent_session_lifetime = timedelta(days=Config.session_lifetime)


# create data directory where posts are stored
try:
    mkdir(Config.data_dir)
except FileExistsError:
    pass
