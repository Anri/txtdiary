from os import environ as envar

from flask import session

VAR_USERNAME = "TD_USERNAME"
VAR_USERPASS = "TD_USERPASS"
VAR_PRIVATE = "TD_PRIVATE"
VAR_LOGLIFETIME = "TD_LOGINLIFETIME"
VAR_BASE = "TD_BASEROUTE"


class User:
    """user informations"""

    # user name
    name = envar[VAR_USERNAME] if VAR_USERNAME in envar else None

    # user password
    password = envar[VAR_USERPASS]


class Config:
    """app configuration"""

    # app name
    name = "txtdiary"

    # app description
    description = "Personal diary page"

    # user
    user = User

    # where is stored the password info
    _session_login = "logged_in"

    # where is stored the timezone info
    _session_timezone = "timezone"

    # data location
    data_dir = "data"

    # turn posts private
    private = True if VAR_PRIVATE in envar else False

    # session duration in days
    session_lifetime = (
        float(envar[VAR_LOGLIFETIME]) if VAR_LOGLIFETIME in envar else 7.0
    )

    # base
    base = envar[VAR_BASE] if VAR_BASE in envar else "/"

    @staticmethod
    def sanitized_base() -> str:
        """Sanitized base used in templates"""
        return "" if Config.base == "/" else Config.base

    @staticmethod
    def is_logged() -> bool:
        """where the info about connection is stored"""
        return (
            session[Config._session_login]
            if Config._session_login in session
            else False
        )

    @staticmethod
    def loggin_in() -> None:
        """where the info about connection is stored"""
        session[Config._session_login] = True
