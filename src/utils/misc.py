from datetime import UTC, datetime
from os import listdir, utime
from os import path as os_path
from os import remove as os_remove
from time import ctime

from flask import session
from pytz import timezone

from utils.config import Config


def post_filename(number: int) -> str:
    """get filename of post"""
    return f"{Config.data_dir}/{number}.txt"


def post_file_id(filename: str) -> int:
    """extract the id from the filename"""
    return int(filename.split("/")[-1][:-4])


def get_posts() -> list[str]:
    """get posts list"""
    posts = [
        os_path.join(Config.data_dir, basename) for basename in listdir(Config.data_dir)
    ]

    # sort from new to old
    posts.sort(key=post_file_id, reverse=True)

    return posts


class File:
    """file representation"""

    def __init__(self, content: str, cdate: str):
        # content
        self.content = content

        # date of creation
        self.creation_date = cdate


def get_post(filename: str) -> File | None:
    """get a post"""
    try:
        open(filename)
    except FileNotFoundError:
        return None
    else:
        with open(filename, "r") as reader:
            return File(reader.read(), get_post_cdatetime(filename))


def get_post_cdatetime(filename: str) -> str:
    return ctime(os_path.getmtime(filename))


def get_post_cdate(filename: str) -> str:
    dt = datetime.fromtimestamp(os_path.getmtime(filename))

    return dt.strftime("%a %b %d %Y")


def delete_post(filename: str) -> bool:
    """delete a post"""
    try:
        open(filename)
    except FileNotFoundError:
        return False
    else:
        os_remove(filename)
        return True


def fresh_file_id() -> int:
    # finding all posts
    paths = get_posts()

    # use the latest file as the reference and increment the ID
    filename = 0
    if len(paths) > filename:
        chosen = max(iter(paths), key=post_file_id)

        # add 1 to the chosen id
        filename = post_file_id(chosen) + 1
    return filename


def exist_post(file_id: int) -> float | None:
    """check if post exists, return modification epoch"""
    try:
        return os_path.getmtime(post_filename(file_id))
    except FileNotFoundError:
        return None


def time_now() -> float:
    """get current time based on timezone without being timezone aware"""
    now = datetime.now(UTC).replace(tzinfo=None)
    return (
        now.timestamp()
        + timezone(session[Config._session_timezone]).utcoffset(now).total_seconds()
    )


def create_post(file_id: int, content: str) -> bool:
    """create a post"""
    # check if the file already exists
    update = exist_post(file_id)

    filename = post_filename(file_id)

    # write into the file
    with open(filename, "w", encoding="utf-8") as f:
        f.write(content)

    if update:
        # keep metadata
        utime(filename, (update, update))
        return True
    else:
        # use user's tz
        now = time_now()
        utime(filename, (now, now))
        return False
