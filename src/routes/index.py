from flask import Blueprint, flash, redirect, render_template, request
from werkzeug import Response

from utils.config import Config
from utils.misc import create_post, fresh_file_id, get_post_cdate, get_posts

name = __name__.split(".")[-1]
router = Blueprint(name, __name__)


@router.route("/")
def index() -> str:
    """index page"""
    posts = []
    if not Config.private or (Config.private and Config.is_logged()):
        for p in get_posts():

            def titleify(s):
                return s.split("/")[-1]

            def linkify(s):
                return ".".join(titleify(s).split(".")[:-1])

            posts.append((linkify(p), titleify(p), get_post_cdate(p)))

    return render_template(
        "index.html",
        config=Config,
        page_name=name,
        posts=posts,
    )


@router.route("/", methods=["POST"], defaults={"post_id": None})
@router.route("/<post_id>", methods=["POST"])
def new_post(post_id: int | None) -> Response:
    """create a new post"""
    if Config.is_logged():
        content = request.form.get("p")
        if content:
            if post_id is None:
                filename = fresh_file_id()
            else:
                filename = post_id

            # create/update the post
            update = create_post(filename, content)

            flash(
                f"<a href='{Config.sanitized_base()}/read/{filename}'>post {'updated' if update else 'created'}</a>."
            )
        else:
            flash(f"invalid post: {content}")
    else:
        flash("you can't do that.")

    return redirect(Config.base)
