from flask import Blueprint, flash, redirect, render_template
from werkzeug import Response

from utils.config import Config
from utils.misc import delete_post, get_post, post_file_id, post_filename

name = __name__.split(".")[-1]
router = Blueprint(name, __name__)


@router.route(f"/{name}/<int:file>")
def read(file: int) -> str:
    """read page"""
    post = None
    if not Config.private or (Config.private and Config.is_logged()):
        filename = post_filename(file)
        post = get_post(filename)

    return render_template(
        "read.html",
        config=Config,
        read_page=True,
        page_name=name,
        name=f"{file}.txt",
        file=post.content.split("\n") if post else None,
        post_file_id=post_file_id,
        date=post.creation_date if post else None,
    )


@router.route(f"/{name}/<int:file>", methods=["POST"])
def remove_post(file: int) -> Response:
    """remove a post"""
    if Config.is_logged():
        filename = post_filename(file)
        if delete_post(filename):
            flash(f"{filename} deleted.")
        else:
            flash(f"{filename} doesn't exists")

    return redirect(Config.base)
