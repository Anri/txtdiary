from flask import Blueprint, jsonify, request, session
from werkzeug import Response

from utils.config import Config

name = __name__.split(".")[-1]
router = Blueprint(name, __name__)


@router.route(f"/{name}", methods=["POST"])
def set_timezone() -> Response:
    """set timezone"""
    timezone = request.data.decode("utf-8")
    session[Config._session_timezone] = timezone

    return jsonify(success=True)
