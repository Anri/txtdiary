from flask import Blueprint, flash, redirect, render_template, request
from werkzeug import Response

from utils.config import Config

name = __name__.split(".")[-1]
router = Blueprint(name, __name__)


@router.route(f"/{name}")
def login() -> str | Response:
    """login page"""
    if Config.is_logged():
        flash("already logged.")
        return redirect(Config.base)

    return render_template(
        "login.html", config=Config, page_name=name, login_page=True, print=print
    )


@router.route(f"/{name}", methods=["POST"])
def check() -> Response:
    """login logic"""
    if not Config.is_logged():
        if request.form["password"] == Config.user.password:
            Config.loggin_in()
            flash("logged.")
        else:
            flash("wrong password.")
    else:
        flash("already logged.")

    return redirect(Config.base)
