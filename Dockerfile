# syntax=docker/dockerfile:1

FROM python:3.11.6-alpine3.18

RUN addgroup -S td && adduser -S td -G td

WORKDIR /txtdiary
RUN mkdir ./data && chown td:td ./data

COPY LICENSE .

COPY requirements.txt .
RUN pip install pyuwsgi -r requirements.txt

COPY ./src ./app

USER td


CMD [ "uwsgi", \
  "--http", "0.0.0.0:80", "-M", \
  "-w", "app", \
  "--callable", "app", \
  "--pythonpath", "/txtdiary/app" ]
