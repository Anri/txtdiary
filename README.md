# txtdiary

[![status-badge](https://git.mylloon.fr/Anri/txtdiary/badges/workflows/publish.yml/badge.svg)](https://git.mylloon.fr/Anri/txtdiary/actions?workflow=publish.yml)

personal diary with txt style, can be private!

## environment variables

|        name        |             description             |    required     |
| :----------------: | :---------------------------------: | :-------------: |
|   `TD_USERNAME`    |              user name              |       no        |
|   `TD_USERPASS`    |            user password            |       yes       |
|    `TD_PRIVATE`    |           post visibility           | empty = `false` |
| `TD_LOGINLIFETIME` |        login lifetime (days)        |   empty = `7`   |
|   `TD_BASEROUTE`   | base route, must start with a slash |       no        |

## volumes (docker)

|       path       |      description       |
| :--------------: | :--------------------: |
| `/txtdiary/data` | where posts are stored |

## local run

for example, using `pass` password:

```sh
$ pip install -r requirements.txt
$ TD_USERPASS=pass TZ=UTC FLASK_APP=src/app flask run
```
